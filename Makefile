## Try to a generic Makefile
## I will rely on environment variables 
## to make it general

## Probably should think about setup.py rather than a makefile
## you can override any of these variables, by, for example
## make F2PY=f2py2 ATLASLIB="-llapack -lblas -latlas -L. -lmatvec -ltestqr" all
## it might be convenient to specify an alias
## eg alias amake='make F2PY=f2py2 ATLASLIB="-llapack -lblas -latlas -L. -lmatvec -ltestqr"'
## then you can make all without editing this file

##at work, the command might look like:
# ATLASLIB="-L/opt/sw/fw/rsc/atlas/3.8.3/lib/ -llapack -lcblas -lf77blas -latlas"
# make ATLASLIB="${ATLASLIB} -lgfortran" F77=ifort unifilter
# quotes are important

## to do the parallel stuff, using ifort, you might try:
## make ATLASLIB="${ATLASLIB} -lgfortran" F77=ifort \
##   F77FLAGS='-openmp -D__OPENMP=200805' \
##   EXTRA_LIBS='-liomp5 -lpthread -lifcoremt' F2PYFLAGS='' mixture

## to use gfortran,
## amake ATLASLIB="${ATLASLIB} -lgfortran" \
## F77=gfortran F77FLAGS='-fopenmp -D__OPENMP=200805' \
## EXTRA_LIBS='-lgomp' F2PYFLAGS=''  mixture

## Chris Add to .bashrc
## alias pmake='make F2PY=f2py2 ATLASLIB="-llapack -lblas -latlas -lgfortran -L. -lmatvec -ltestqr" \
## F77=gfortran F77FLAGS="-fopenmp -D__OPENMP=200805" \
## EXTRA_LIBS="-lgomp" F2PYFLAGS=""'  

#Chris uses
#F2PY = f2py2
F2PY = f2py

F2PYFLAGS = "-Ofast -march=native -funroll-loops  -fwhole-program -flto -msse3 -fbounds-check"

#F77 should be set already, but use gfortran if not
F77 = gfortran
#ATLASLIB = -llapack_atlas -lblas
ATLASLIB = -llapack -lblas -latlas

#Chris:
#ATLASLIB = -llapack -lblas -latlas -L. -lmatvec -ltestqr

all: matvec testqr conjugate carterkohn unifilter filter dfm_utils \
	 filterreg sur factor sfactor amux2 nbfun conregc \
         conjugate2

# QL.f not found, so not putting QL into all yet

amux2: amux2.f
	$(F2PY) -c amux2.f -m amux2 ${ATLASLIB} \
		--f77flags="${F77FLAGS}" \
		 --opt=${F2PYFLAGS}  ${EXTRA_LIBS}  
# RB: Updated 21/9/2011	
	
carterkohn: carterkohn.f
	$(F2PY) -c carterkohn.f -m carterkohn

conregc: conregc.f
	$(F2PY) -c conregc.f -m conreg \
        $(ATLASLIB) -lgomp --f77flags="-fopenmp" --opt=${F2PYFLAGS}

conjugate: conjugate.f
	$(F2PY) -c conjugate.f testqr.f -m conjugate $(ATLASLIB) -L. -lmatvec --opt=${F2PYFLAGS}

conjugate2: conjugate2.f
	$(F2PY) -c conjugate2.f testqr.f -m conjugate2 $(ATLASLIB) -L. -lmatvec --opt=${F2PYFLAGS}

dfm_utils: dfm_utils.f
	$(F2PY) -c dfm_utils.f -m dfm_utils $(ATLASLIB) --opt=${F2PYFLAGS}

factor: factor.f
	$(F2PY) -c factor.f -m factor $(ATLASLIB) \
        --f77flags="${F77FLAGS}" \
         --opt=${F2PYFLAGS} ${EXTRA_LIBS}

filter: filter.f
	$(F2PY) -c filter.f -m filter $(ATLASLIB) \
        --f77flags="${F77FLAGS}" \
         --opt=${F2PYFLAGS} ${EXTRA_LIBS}


filterreg: filterreg.f
	$(F2PY) -c filterreg.f -m filterreg $(ATLASLIB) \
        --f77flags="${F77FLAGS}" \
         --opt=${F2PYFLAGS} ${EXTRA_LIBS}

matvec: matvec.f
	$(F77) -c -fPIC matvec.f
	$(F77) -shared matvec.o -o libmatvec.so
	$(F2PY) -c matvec.f -m matvec --opt=${F2PYFLAGS}

nbfun: nbfun.f90
	$(F2PY) -c nbfun.f90 -m nbfunf

QL: QL.f
	$(F2PY) -c QL.f -m QL $(ATLASLIB) --opt=${F2PYFLAGS}

stochsearch: stochsearch.f
	$(F2PY) -c stochsearch.f -m stochsearch ${ATLASLIB} --opt=${F2PYFLAGS}

sfactor: sfactor.f
	$(F2PY) -c sfactor.f -m sfactor ${ATLASLIB} \
		--f77flags="${F77FLAGS}" \
		 --opt=${F2PYFLAGS}  ${EXTRA_LIBS}            
# RB: Updated 30/8/2011		 

sur: sur.f
	$(F2PY) -c sur.f -m SUR_fort ${ATLASLIB} --opt=${F2PYFLAGS}

testqr: testqr.f
	$(F77) -c -fPIC testqr.f
	$(F77) -shared testqr.o -o libtestqr.so $(ATLAS) -L.-lmatvec
	$(F2PY) -c testqr.f -m testqr $(ATLAS) -L.-lmatvec

timeseries: timeseriesfunc.f
	$(F2PY) -c timeseriesfunc.f -m timeseriesfunc --opt=${F2PYFLAGS}

unifilter: univariate.f
	$(F2PY) -c univariate.f -m unifilter $(ATLASLIB) \
        --f77flags="${F77FLAGS}" \
         --opt=${F2PYFLAGS} ${EXTRA_LIBS}
 
reducedc: reduced_cond.f
	$(F2PY) -c reduced_cond.f -m reducedc ${ATLASLIB} --opt=${F2PYFLAGS}

ssm_utils: ssm_utils.f
	$(F2PY) -c ssm_utils.f -m ssm_utilsf ${ATLASLIB} --opt=${F2PYFLAGS}

SV: SV.f
	$(F2PY) -c SV.f -m svf $(ATLASLIB) \
        --f77flags="${F77FLAGS}" \
         --opt=${F2PYFLAGS} ${EXTRA_LIBS}

#testparallel: testparallel.f
#    $(F77) -c filter.f
#    $(F77) -c testparallel.f
#    $(F77) -c filter.o testparallel.o -o testp 
#        --f77flags="${F77FLAGS}" \
#         --opt=${F2PYFLAGS} ${EXTRA_LIBS}


#multinomial: multinomial.f
#	$(F2PY) -c multinomial.f -m multinomial -llapack -lblas -latlas --opt=${F2PYFLAGS}

# randf2pyf:randf2py.f
# 	gfortran -c -fPIC -fno-range-check randf2py.f
# 	gfortran -shared randf2py.o -o librandf2py.so
# 
# random: random.f
# 	$(F2PY) -c random.f -m randomf -L. -lrandf2py
