#import libraries
import numpy as np
from pyssm.ssm import Filter
import matplotlib.pyplot as plt

#Set seed for random number generator
np.random.seed(12345)

#Define system dimensions
nobs = 1000; nseries = 1; nstate = 2; rstate = 2

#Define system matricies
zt = np.array(((1., 0.)))
rt = 1.; ht = 20. ** 2
tt = np.array(((1., 1.), (0., 1.)))
gt = np.eye(2); p1 = np.eye(2) * 0.1 ** 2
qt = np.eye(2) * 0.1 ** 2
a1 = np.array((5., 0.1))

#Define filter class
timevar = False #No timevarying system matrices
filt = Filter(nobs, nseries, nstate, rstate, timevar)


#Initialise system matrices
filt.initialise_system(a1, p1, zt, ht, tt, gt, qt, rt)
filt.simssm() # Simulate data

yvec = filt.get_ymat().T.flatten() #Get simulated data
state = filt.get_state() #Get simulated state

#Plot simulated data and state
fig, axes = plt.subplots(nrows = 3, ncols = 1)
fig.tight_layout()
plt.subplot(3,1,1)
plt.plot(yvec)
plt.title("Simulated Data")
plt.subplot(3,1,2)
plt.plot(state[0])
plt.title("Simulated trend")
plt.subplot(3,1,3)
plt.plot(state[1])
plt.title("Simulated slope")
plt.show()


