#import libraries
import numpy as np
from pyssm.ssm import Filter, SimSmoother
from pymcmc.regtools import CondScaleSampler


#Set seed for random number generator
np.random.seed(12345)

def simdata():
    "function simulates data"

    nobs = 1000 #1000 observations
    nseries = 1# Number of time series
    nreg = 3 #Number of regressors
    nstate = rstate = 1 #state vector and covariance dimension

    #Define xmat; where xmat[:,:,t] - X(t)
    xmat = np.random.randn(nseries, nreg, nobs)
    xmat[:,0,:] = 1. #Set first regressor as constant
    beta = np.random.randn(nreg)

    #Define system matrices
    zt = 1.; rt = 0.; ht = 1.; a1 = 0.; gt = 1.
    qt = 0.3 ** 2
    tt = 0.95
    p1 = qt / (1. - tt ** 2)


    #Define filter class
    timevar = False #No timevarying system matrices
    filt = Filter(nobs, nseries, nstate, rstate, timevar, xmat = xmat)
    filt.initialise_system(a1, p1, zt, ht, tt, gt, qt, rt, beta = beta)
    filt.simssm() #Simulate data

    yvec = filt.get_ymat().T.flatten() #Get simulated data
    state = filt.get_state() #Get simulated state

    return yvec, xmat, state

def update_tt_p1(store):
    "Function updates tt and p1"
    system = store['simsm'].get_system()
    system.update_tt(store['phi'])
    system.update_p1(store['qt'] / (1. - store['phi'] ** 2))

def simstate(store):
    "Function simulates state"

    #Note block after RWMH for phi so only need to 
    #Update here
    update_tt_p1(store)

    return store['simsm'].sim_smoother()

def sim_qt(store):
    "Function to simulate qt"

    system = store['simsm'].get_system()
    res = store['simsm'].get_state_residual()

    #Sample  qt
    qt = store['scale_sampler'].sample(res, 1) ** 2
    system.update_qt(qt)
    return qt

def prior_phi(phi):
    "return log of beta distribution."

    alpha = 15.
    beta = 1.5

    return (alpha - 1.) * np.log(phi) + (beta - 1.) * np.log(1. - phi)

def log_likelihood(store):
    "Function returns the log-likelihood function."

    update_tt_p1(store) #updates tt (Note likelihood function is only being
                        #used in RWMH algorithm for phi, so only tt and initial p1 need to be updated

    return store['simsm'].log_likelihood()

def posterior_phi(store):
    "Function returns the log posterior for phi"

    if store['phi'] > 0 and store['phi'] < 1.:
        #Note only evaluate if phi is in stationary region
        return log_likelihood(store) + prior_phi(store['phi'])
    else:
        return -1E256 #Effective 0 probability (Numerical truncation)

def main():
    "Main function"

    #simulate data
    yvec, xmat, simulated_state = simdata()

    data = {'yvec': yvec}

    #Initial values for system matrices
    zt = 1.; rt = 0.; ht = 1.; a1 = 0.; gt = 1.
    qt = 0.2 ** 2
    tt = 0.9
    p1 = qt / (1. - tt ** 2)

    data['simsm'] = SimSmoother(yvec, 1, 1, False, xmat = xmat)
    #Note no argument implies beta is initialised at 0
    data['simsm'].initialise_system(a1, p1, zt, ht, tt, gt, qt, rt)
    data['scale_sampler'] = CondScaleSampler(prior = ['inverted_gamma', 10, 0.1])

    #Define 





